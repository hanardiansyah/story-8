from django.urls import include,path
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('', views.landing, name = 'index'),
    path('login', views.user_login,name='login'),
    path('logout', views.user_logout,name='logout'),
]
