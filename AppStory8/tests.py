from django.test import TestCase, LiveServerTestCase, RequestFactory
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from . import views
from django.contrib.auth import authenticate
import time

#from .views import search

from django.test import TestCase, Client

# Create your tests here.
class Story8UnitTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
        username='jacob', email='jacob@…', password='top_secret')

    def test_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


    def test_page_uses_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_user(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        c = Client()
        logged_in = c.login(username='testuser', password='112345')

    def test_details(self):
        request = self.factory.get('/login/')
        request.user = self.user
        response = views.user_login(request)
        userlog = authenticate(request.user)
        self.assertEqual(response.status_code, 200)

class Story8FunctionalTest(LiveServerTestCase):

    def setUp(self):
        #chrome
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=r'./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.refresh()
        self.browser.quit()
        super(Story8FunctionalTest, self).tearDown()

    #def test_login(self):
        #self.browser.get('http://127.0.0.1:8000/login')

    def test_login(self):
        self.browser.get('http://127.0.0.1:8000/login')
        time.sleep(5)
        new_username = self.browser.find_element_by_id('username')
        new_password = self.browser.find_element_by_id('password')
        submit = self.browser.find_element_by_id('submit')
        new_username.send_keys('tamuhana')
        new_password.send_keys('hana1234')
        submit.click()
        time.sleep(10)
        logout = self.browser.find_element_by_id('logoutButt')
        logout.click()