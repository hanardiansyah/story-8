from django import forms


class SearchForm(forms.Form):
    search_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Search book',
    }
    title = forms.CharField(max_length = 300, label='', widget=forms.TextInput(attrs=search_attrs))
