$(document).ready(function(){
  $("button").click(
    function(){
      var input = $("#searchBox").val()
      console.log("run", input)
      $.ajax({
        method: "GET",
        url: "https://www.googleapis.com/books/v1/volumes?q="+input,
        success: function(result){
          $("table").empty();
          $("table").append(
            '<thead>' +
              '<tr>' +
                '<th scope="col">Book Picture</th>' +
                '<th scope="col">Book Title</th>' +
                '<th scope="col">Book Author</th>' +
              '</tr>'+
            '</thead>'
          );
          for(i = 0; i < result.items.length ; i++){
            $("table").append(
              "<tbody>" +
                "<tr>" +
                  "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                  "<td>" + result.items[i].volumeInfo.title + "</td>" +
                  "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                "</tr>" +
              "</tbody>"
            );
          }
        }
      })
    }
  );
})

$("input").change(
  function(){
    console.log("run run")
  }
);
